import 'package:get/get.dart';

import '../modules/home/bindings/home_binding.dart';
import '../modules/home/views/home_view.dart';
import '../modules/opd/bindings/opd_binding.dart';
import '../modules/opd/views/opd_view.dart';
import '../modules/rsudsmc/bindings/rsudsmc_binding.dart';
import '../modules/rsudsmc/views/rsudsmc_view.dart';
import '../modules/rsudsmc_diagnosa_icd10/bindings/rsudsmc_diagnosa_icd10_binding.dart';
import '../modules/rsudsmc_diagnosa_icd10/views/rsudsmc_diagnosa_icd10_view.dart';
import '../modules/rsudsmc_hisubdist/bindings/rsudsmc_hisubdist_binding.dart';
import '../modules/rsudsmc_hisubdist/views/rsudsmc_hisubdist_view.dart';
import '../modules/rsudsmc_lowsubdist/bindings/rsudsmc_lowsubdist_binding.dart';
import '../modules/rsudsmc_lowsubdist/views/rsudsmc_lowsubdist_view.dart';
import '../modules/rsudsmc_patientvisits/bindings/rsudsmc_patientvisits_binding.dart';
import '../modules/rsudsmc_patientvisits/views/rsudsmc_patientvisits_view.dart';
import '../modules/rsudsmc_poliklinik/bindings/rsudsmc_poliklinik_binding.dart';
import '../modules/rsudsmc_poliklinik/views/rsudsmc_poliklinik_view.dart';
import '../modules/rsudsmc_rawatkelasinap/bindings/rsudsmc_rawatkelasinap_binding.dart';
import '../modules/rsudsmc_rawatkelasinap/views/rsudsmc_rawatkelasinap_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => const HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.RSUDSMC,
      page: () => RsudsmcView(),
      binding: RsudsmcBinding(),
    ),
    GetPage(
      name: _Paths.RSUDSMC_HISUBDIST,
      page: () => const RsudsmcHisubdistView(),
      binding: RsudsmcHisubdistBinding(),
    ),
    GetPage(
      name: _Paths.RSUDSMC_PATIENTVISITS,
      page: () => RsudsmcPatientvisitsView(),
      binding: RsudsmcPatientvisitsBinding(),
    ),
    GetPage(
      name: _Paths.RSUDSMC_LOWSUBDIST,
      page: () => const RsudsmcLowsubdistView(),
      binding: RsudsmcLowsubdistBinding(),
    ),
    GetPage(
      name: _Paths.RSUDSMC_POLIKLINIK,
      page: () => const RsudsmcPoliklinikView(),
      binding: RsudsmcPoliklinikBinding(),
    ),
    GetPage(
      name: _Paths.RSUDSMC_RAWATKELASINAP,
      page: () => const RsudsmcRawatkelasinapView(),
      binding: RsudsmcRawatkelasinapBinding(),
    ),
    GetPage(
      name: _Paths.RSUDSMC_DIAGNOSA_ICD10,
      page: () => const RsudsmcDiagnosaIcd10View(),
      binding: RsudsmcDiagnosaIcd10Binding(),
    ),
    GetPage(
      name: _Paths.OPD,
      page: () => const OpdView(),
      binding: OpdBinding(),
    ),
  ];
}
