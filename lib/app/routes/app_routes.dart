part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();
  static const HOME = _Paths.HOME;
  static const RSUDSMC = _Paths.RSUDSMC;
  static const RSUDSMC_HISUBDIST = _Paths.RSUDSMC_HISUBDIST;
  static const RSUDSMC_PATIENTVISITS = _Paths.RSUDSMC_PATIENTVISITS;
  static const RSUDSMC_LOWSUBDIST = _Paths.RSUDSMC_LOWSUBDIST;
  static const RSUDSMC_POLIKLINIK = _Paths.RSUDSMC_POLIKLINIK;
  static const RSUDSMC_RAWATKELASINAP = _Paths.RSUDSMC_RAWATKELASINAP;
  static const RSUDSMC_DIAGNOSA_ICD10 = _Paths.RSUDSMC_DIAGNOSA_ICD10;
  static const OPD = _Paths.OPD;
}

abstract class _Paths {
  _Paths._();
  static const HOME = '/home';
  static const RSUDSMC = '/rsudsmc';
  static const RSUDSMC_HISUBDIST = '/rsudsmc-hisubdist';
  static const RSUDSMC_PATIENTVISITS = '/rsudsmc-patientvisits';
  static const RSUDSMC_LOWSUBDIST = '/rsudsmc-lowsubdist';
  static const RSUDSMC_POLIKLINIK = '/rsudsmc-poliklinik';
  static const RSUDSMC_RAWATKELASINAP = '/rsudsmc-rawatkelasinap';
  static const RSUDSMC_DIAGNOSA_ICD10 = '/rsudsmc-diagnosa-icd10';
  static const OPD = '/opd';
}
