import 'package:get/get.dart';

import '../controllers/rsudsmc_poliklinik_controller.dart';

class RsudsmcPoliklinikBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RsudsmcPoliklinikController>(
      () => RsudsmcPoliklinikController(),
    );
  }
}
