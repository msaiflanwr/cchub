import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../controllers/rsudsmc_poliklinik_controller.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:splp_demo/app/routes/app_pages.dart';

class RsudsmcPoliklinikView extends GetView<RsudsmcPoliklinikController> {
  const RsudsmcPoliklinikView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Kunjungan Pasien Poliklinik'),
        titleTextStyle:
            GoogleFonts.poppins(fontSize: 16, fontWeight: FontWeight.w600),
        centerTitle: true,
        backgroundColor: Color(0xff6276E9),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_new_rounded,
            size: 14,
          ),
          onPressed: () {
            Get.offAllNamed(Routes.RSUDSMC);
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 30, left: 40, right: 40),
        child: ListView(
          children: [
            Text(
              "Data Kunjungan Pasien\nBerdasarkan Kunjungan Poliklinik",
              style: GoogleFonts.poppins(
                  fontSize: 12, fontWeight: FontWeight.w600),
              textAlign: TextAlign.center,
            ),
            SizedBox(
              height: 20,
            ),
            // ...
            Obx(() => Container(
                  height: 50,
                  child: TextField(
                    onTap: () => _selectDate(context, 'date1'),
                    readOnly: true,
                    controller:
                        TextEditingController(text: controller.date1.value),
                    decoration: InputDecoration(
                      labelText: 'Tanggal Awal',
                      labelStyle: GoogleFonts.poppins(fontSize: 12),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 20),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Icon(
                          Icons.calendar_today,
                          size: 16,
                        ),
                      ),
                    ),
                  ),
                )),
            SizedBox(
              height: 10,
            ),
            // ...
            Obx(() => Container(
                  height: 50,
                  child: TextField(
                    onTap: () => _selectDate(context, 'date2'),
                    readOnly: true,
                    controller:
                        TextEditingController(text: controller.date2.value),
                    decoration: InputDecoration(
                      labelText: 'Tanggal Akhir',
                      labelStyle: GoogleFonts.poppins(fontSize: 12),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30),
                      ),
                      contentPadding: EdgeInsets.symmetric(horizontal: 20),
                      suffixIcon: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Icon(
                          Icons.calendar_today,
                          size: 16,
                        ),
                      ),
                    ),
                  ),
                )),
            SizedBox(
              height: 20,
            ),
            // ...
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                SizedBox(
                  height: 40,
                  child: ElevatedButton(
                    onPressed: () {
                      controller.callApi();
                    },
                    child: Center(
                      child: Text(
                        'Ambil Data',
                        textAlign: TextAlign.center,
                        style: GoogleFonts.poppins(
                            fontSize: 12, fontWeight: FontWeight.w600),
                      ),
                    ),
                    style: ButtonStyle(
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Color(0xff6276E9)),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                SizedBox(
                  height: 40,
                  width: 80,
                  child: OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                        side: BorderSide(color: Colors.grey),
                      ),
                      onPressed: () {
                        Get.offAllNamed(Routes.RSUDSMC_POLIKLINIK);
                      },
                      child: Text(
                        "Reset",
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 12,
                        ),
                      )),
                ),
              ],
            ),

            // ...
            Obx(() {
              if (controller.isLoading.value) {
                return CircularProgressIndicator();
              } else {
                return Container();
              }
            }),

            // Table to display the data
            Padding(
              padding: const EdgeInsets.only(top: 16),
              child: Divider(
                color: Colors.grey[300],
                thickness: 1,
              ),
            ),
            SizedBox(height: 20),
            Obx(
              () => Table(
                border: TableBorder.all(
                  color: Colors.grey,
                  borderRadius: BorderRadius.circular(10),
                ),
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                columnWidths: {0: FixedColumnWidth(40)},
                children: [
                  TableRow(
                    children: [
                      TableCell(
                        child: SizedBox(
                          child: Container(
                            decoration: BoxDecoration(
                              color: Color(0xff6276E9),
                              borderRadius: BorderRadius.only(
                                topLeft: Radius.circular(10),
                              ),
                            ),
                            alignment: Alignment.center,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'No.',
                                style: GoogleFonts.poppins(
                                  fontSize: 12,
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      TableCell(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color(0xff6276E9),
                          ),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Poliklinik',
                              style: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                      TableCell(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color(0xff6276E9),
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(10),
                            ),
                          ),
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Jumlah',
                              style: GoogleFonts.poppins(
                                fontSize: 12,
                                fontWeight: FontWeight.w600,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  // Generate rows dynamically based on API response or display "Data Kosong" if dataList is empty
                  ...List.generate(
                    controller.dataList.isNotEmpty ? controller.dataLength : 1,
                    (index) => TableRow(
                      children: [
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: Text(
                                '${index + 1}.',
                                style: GoogleFonts.poppins(fontSize: 12),
                              ),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              controller.dataList.isEmpty
                                  ? '-'
                                  : controller.getData(index).departemen,
                              style: GoogleFonts.poppins(fontSize: 12),
                            ),
                          ),
                        ),
                        TableCell(
                          child: Center(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                controller.dataList.isEmpty
                                    ? '-'
                                    : controller.getData(index).formattedJumlah,
                                style: GoogleFonts.poppins(fontSize: 12),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),

            SizedBox(
              height: 8,
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 30),
              child: Center(
                child: Text(
                  "Command Center Hub Kab.Tasikmalaya",
                  style: GoogleFonts.poppins(fontSize: 8),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _selectDate(BuildContext context, String dateField) async {
    final DateTime? pickedDate = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2100),
      builder: (context, child) {
        return Theme(
          data: ThemeData(
              colorScheme:
                  ColorScheme.light().copyWith(primary: Color(0xff6276E9))),
          child: child!,
        );
      },
    );
    if (pickedDate != null) {
      final formattedDate = pickedDate.toString().substring(0, 10);
      if (dateField == 'date1') {
        controller.setDate1(formattedDate);
      } else if (dateField == 'date2') {
        controller.setDate2(formattedDate);
      }
    }
  }
}
