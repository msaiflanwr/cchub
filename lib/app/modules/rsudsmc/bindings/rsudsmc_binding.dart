import 'package:get/get.dart';

import '../controllers/rsudsmc_controller.dart';

class RsudsmcBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RsudsmcController>(
      () => RsudsmcController(),
    );
  }
}
