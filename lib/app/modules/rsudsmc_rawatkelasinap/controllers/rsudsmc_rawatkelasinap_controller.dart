import 'dart:convert';

import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class RsudsmcRawatkelasinapController extends GetxController {
  var date1 = ''.obs;
  var date2 = ''.obs;
  var isLoading = false.obs;
  var dataList = <VisitData>[].obs;

  int get dataLength => dataList.length;
  VisitData getData(int index) => dataList[index];

  void setIsLoading(bool value) {
    isLoading.value = value;
  }

  void setDate1(String value) {
    date1.value = value;
  }

  void setDate2(String value) {
    date2.value = value;
  }

  Future<void> callApi() async {
    Get.snackbar("Mohon tunggu", "API sedang berjalan...");
    print("API sedang berjalan..");

    var url =
        Uri.parse('https://api.rsudsmc.id:8888/dashboard/inpatient_visits');
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': 'wssmc=pjcv351aogk2k635u80npe9hpn'
    };
    var body = {
      'x-username': 'bridkemenkes',
      'x-password': 'br1dk3m3nk3s',
      'tgl_awal': date1.value,
      'tgl_akhir': date2.value,
    };

    var response = await http.post(url, headers: headers, body: body);
    print("API sudah selesai memberikan data ke HP Anda");
    print('Response Code Sukses: ${response.statusCode}');
    print('Response Data nya: ${response.body}');

    setIsLoading(true);

    if (response.statusCode == 200) {
      Get.snackbar("Sukses",
          "Kunjungan Pasien Berdasarkan Ruang Rawat Kelas Inap Berhasil Ditampilkan");

      var jsonResponse = json.decode(response.body);
      if (jsonResponse is List<dynamic>) {
        dataList.clear();
        dataList.addAll(
          jsonResponse.map(
            (item) => VisitData(item['ruangan'], double.parse(item['jumlah'])),
          ),
        );
      }
    } else {
      Get.snackbar("Terjadi Kesalahan", "Silahkan Coba Beberapa Saat Lagi");
    }

    setIsLoading(false);
  }
}

class VisitData {
  final String ruangan;
  final double jumlah;

  VisitData(this.ruangan, this.jumlah);

  String get formattedJumlah => jumlah.toStringAsFixed(0);
}
