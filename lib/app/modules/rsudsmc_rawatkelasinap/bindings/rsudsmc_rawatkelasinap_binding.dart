import 'package:get/get.dart';

import '../controllers/rsudsmc_rawatkelasinap_controller.dart';

class RsudsmcRawatkelasinapBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RsudsmcRawatkelasinapController>(
      () => RsudsmcRawatkelasinapController(),
    );
  }
}
