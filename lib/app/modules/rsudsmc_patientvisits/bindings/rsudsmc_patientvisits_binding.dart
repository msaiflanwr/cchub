import 'package:get/get.dart';

import '../controllers/rsudsmc_patientvisits_controller.dart';

class RsudsmcPatientvisitsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RsudsmcPatientvisitsController>(
      () => RsudsmcPatientvisitsController(),
    );
  }
}
