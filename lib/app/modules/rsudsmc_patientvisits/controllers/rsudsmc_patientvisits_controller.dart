import 'dart:convert';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:splp_demo/app/routes/app_pages.dart';

class RsudsmcPatientvisitsController extends GetxController {
  var date1 = ''.obs;
  var date2 = ''.obs;
  var apiResponse = ''.obs;
  var chartData = <Data>[].obs;
  var isLoading = false.obs;
  var rawatJalan = ''.obs;
  var rawatInap = ''.obs;
  var rawatIgd = ''.obs;
  var lakilaki = ''.obs;
  var perempuan = ''.obs;
  var kunjBaru = ''.obs;
  var kunjLama = ''.obs;
  var kunjUmum = ''.obs;
  var kunjBPJS = ''.obs;
  var asuransi = ''.obs;

  void setIsLoading(bool value) {
    isLoading.value = value;
  }

  void setDate1(String value) {
    date1.value = value;
  }

  void setDate2(String value) {
    date2.value = value;
  }

  void setApiResponse(String value) {
    apiResponse.value = value;
  }

  void setChartData(List<Data> data) {
    chartData.assignAll(data);
  }

  //KODE UNTUK MENGAMBIL DATA DARI RSUDSMC.id
  //KODE UNTUK MENGAMBIL DATA DARI RSUDSMC.id
  //KODE UNTUK MENGAMBIL DATA DARI RSUDSMC.id

  Future<void> callApi() async {
    Get.snackbar("Mohon tunggu", "API sedang berjalan...");
    print("API sedang berjalan..");

    var url = Uri.parse('https://api.rsudsmc.id:8888/dashboard/patient_visits');
    var headers = {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Cookie': 'wssmc=pjcv351aogk2k635u80npe9hpn'
    };
    var body = {
      'x-username': 'bridkemenkes',
      'x-password': 'br1dk3m3nk3s',
      'tgl_awal': date1.value,
      'tgl_akhir': date2.value,
    };

    //KODE UNTUK MENGAMBIL DATA DARI RSUDSMC.id sampai sini
    //KODE UNTUK MENGAMBIL DATA DARI RSUDSMC.id sampai sini
    //KODE UNTUK MENGAMBIL DATA DARI RSUDSMC.id sampai sini

    var response = await http.post(url, headers: headers, body: body);
    print("API sudah selesai memberikan data ke HP Anda");
    print("API sudah selesai memberikan data ke HP Anda");
    print("API sudah selesai memberikan data ke HP Anda");
    print('Response Code Sukses: ${response.statusCode}');
    print('Response Data nya: ${response.body}');

    final data = json.decode(response.body) as Map<String, dynamic>;

    final rawatJalanValue = data['kunjungan_rawat_jalan'].toString();
    final rawatInapValue = data['kunjungan_rawat_inap'].toString();
    final rawatIgdValue = data['kunjungan_rawat_igd'].toString();
    final lakilakiValue = data['kunjungan_laki_laki'].toString();
    final perempuanValue = data['kunjungan_perempuan'].toString();
    final kunjBaruValue = data['kunjungan_baru'].toString();
    final kunjLamaValue = data['kunjungan_lama'].toString();
    final kunjUmumValue = data['kunjungan_umum'].toString();
    final kunjBPJSValue = data['kunjungan_bpjs'].toString();
    final asuransiValue = data['kunjungan_asuransi'].toString();

    print(rawatJalanValue);
    print(rawatIgdValue);
    print(rawatInapValue);
    print(lakilakiValue);
    print(perempuanValue);
    print(kunjBaruValue);
    print(kunjLamaValue);
    print(kunjUmumValue);
    print(kunjBPJSValue);
    print(asuransiValue);

    // Update nilai variabel pada controller
    rawatJalan.value = rawatJalanValue;
    rawatInap.value = rawatInapValue;
    rawatIgd.value = rawatIgdValue;
    lakilaki.value = lakilakiValue;
    perempuan.value = perempuanValue;
    kunjBaru.value = kunjBaruValue;
    kunjLama.value = kunjLamaValue;
    kunjUmum.value = kunjUmumValue;
    kunjBPJS.value = kunjBPJSValue;
    asuransi.value = asuransiValue;

    setIsLoading(true);

    if (rawatJalan.value == "null") {
      Get.snackbar(
          "Terjadi Kesalahan", "Harap Masukkan Tanggal Terlebih Dahulu");
      Get.offAllNamed(Routes.RSUDSMC_PATIENTVISITS);
    } else {
      Get.snackbar("Sukses", "Kunjungan Pasien Berhasil Ditampilkan");
    }

    // Parsing response body ke dalam List<Data>
    List<Data> dataList = [];
    // ...
    // Lakukan parsing response.body dan tambahkan data ke dalam dataList
    // ...

    // Set data chart
    setChartData(dataList);

    setIsLoading(false);
  }
}

class Data {
  final String category;
  final double value;

  Data(this.category, this.value);
}
