import 'package:get/get.dart';

import '../controllers/rsudsmc_diagnosa_icd10_controller.dart';

class RsudsmcDiagnosaIcd10Binding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RsudsmcDiagnosaIcd10Controller>(
      () => RsudsmcDiagnosaIcd10Controller(),
    );
  }
}
