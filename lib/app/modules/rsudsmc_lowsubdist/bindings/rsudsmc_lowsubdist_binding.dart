import 'package:get/get.dart';

import '../controllers/rsudsmc_lowsubdist_controller.dart';

class RsudsmcLowsubdistBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RsudsmcLowsubdistController>(
      () => RsudsmcLowsubdistController(),
    );
  }
}
