import 'package:get/get.dart';

import '../controllers/opd_controller.dart';

class OpdBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OpdController>(
      () => OpdController(),
    );
  }
}
