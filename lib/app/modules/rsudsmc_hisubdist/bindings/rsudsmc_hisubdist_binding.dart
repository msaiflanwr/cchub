import 'package:get/get.dart';

import '../controllers/rsudsmc_hisubdist_controller.dart';

class RsudsmcHisubdistBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<RsudsmcHisubdistController>(
      () => RsudsmcHisubdistController(),
    );
  }
}
